FROM openjdk:8
LABEL Hirzallah (aysar.hirzallah@progressoft.com)
EXPOSE 9091
ENV SPRING_PROFILE=mysql
COPY . /target/assignment-*.jar /usr/local/app.jar/
RUN chmod +x /usr/local/app.jar
ENTRYPOINT sleep 20; java -jar -Dspring.profiles.active=${SPRING_PROFILE} /usr/local/app.jar

