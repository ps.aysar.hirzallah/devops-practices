export date=$(git show -s --format=%ct | xargs -I{} date -d @{} +%y%m%d)
export short_sha=${CI_COMMIT_SHORT_SHA}
export version=$date-$short_sha
echo $version
